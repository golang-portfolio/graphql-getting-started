package model

import (
	"time"
	"github.com/google/uuid"
)

type Category struct {
	ID     uuid.UUID `json:"id"`
	CreatedOn	time.Time `json:"created_on"`
	UpdatedOn	time.Time `json:"updated_on"`
	DeletedOn	time.Time `json:"deleted_on"`
	CreatedBy	string `json:"created_by"`
	UpdatedBy	string `json:"updated_by"`
	Enterprise	string `json:"enterprise"`
	CategoryName   string `json:"category_name"`
	CategoryCode   string   `json:"category_code"`
}
