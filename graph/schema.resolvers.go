package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"time"

	"github.com/Kengathua/gqlgen-todos/common"
	"github.com/Kengathua/gqlgen-todos/graph/generated"
	"github.com/Kengathua/gqlgen-todos/graph/model"
	"github.com/google/uuid"
)

// ID is the resolver for the id field.
func (r *categoryResolver) ID(ctx context.Context, obj *model.Category) (string, error) {
	return obj.ID.String(), nil
}

// CreateCategory is the resolver for the createCategory field.
func (r *mutationResolver) CreateCategory(ctx context.Context, input model.NewCategory) (*model.Category, error) {
	context := common.GetContext(ctx)
	category := &model.Category{
		ID:           uuid.New(),
		CategoryName: input.CategoryName,
		CategoryCode: input.CategoryCode,
		CreatedOn:    time.Now(),
		UpdatedOn:    time.Now(),
	}
	err := context.Database.Create(&category).Error
	if err != nil {
		return nil, err
	}
	return category, nil
}

// UpdateCategory is the resolver for the updateCategory field.
func (r *mutationResolver) UpdateCategory(ctx context.Context, id string, input model.NewCategory) (*model.Category, error) {
	context := common.GetContext(ctx)

	updateId, err1 := uuid.Parse(id)
	if err1 != nil {
		fmt.Println("Found an error", err1)
	}
	category := &model.Category{
		ID: updateId,
		CategoryName: input.CategoryName,
		CategoryCode: input.CategoryCode,
		UpdatedOn:    time.Now(),
	}
	err := context.Database.Save(&category).Error
	if err != nil {
		return nil, err
	}
	return category, nil
}

// DeleteCategory is the resolver for the deleteCategory field.
func (r *mutationResolver) DeleteCategory(ctx context.Context, id string) (*model.Category, error) {
	context := common.GetContext(ctx)
	var category *model.Category
	err := context.Database.Where("id = ?", id).Delete(&category).Error
	if err != nil {
		return nil, err
	}
	return category, nil
}

// GetOneCategory is the resolver for the GetOneCategory field.
func (r *queryResolver) GetOneCategory(ctx context.Context, id string) (*model.Category, error) {
	context := common.GetContext(ctx)
	var category *model.Category
	err := context.Database.Where("id = ?", id).Find(&category).Error
	if err != nil {
		return nil, err
	}
	return category, nil
}

// GetAllCategories is the resolver for the GetAllCategories field.
func (r *queryResolver) GetAllCategories(ctx context.Context) ([]*model.Category, error) {
	context := common.GetContext(ctx)
	var categories []*model.Category
	err := context.Database.Find(&categories).Error
	if err != nil {
		return nil, err
	}
	return categories, nil
}

// Category returns generated.CategoryResolver implementation.
func (r *Resolver) Category() generated.CategoryResolver { return &categoryResolver{r} }

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type categoryResolver struct{ *Resolver }
type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
